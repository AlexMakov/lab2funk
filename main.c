#include <stdio.h>
#include <string.h>

void input(char *line, int maxLen, int i)
{
    printf("%d: ",i);
    scanf("%s", line);
//    fflush(stdin);
//    scanf("%s", line);
}

void output(char *line)
{
    printf("%s\t", line);
    printf("%d", strlen(line));
    printf("%c", '\n');
}

int sort(char *text[], int lines, int *len)
{
    int swapCount=0;
    char *tmp;
    for (int m = 1; m < lines; m++)
    {
        for (int n = 1; n < lines; n++)
        {
            int a = strlen(text[n]);
            int b = strlen(text[n-1]);
            if(a > *len)
            {
                *len = a;
            }
            if(b > *len)
            {
                *len = b;
            }
            if(a > b)
            {
                tmp=text[n];
                text[n]=text[n-1];
                text[n-1]=tmp;
                swapCount++;
            }
        }
    }
    return swapCount;
}

int main(int argc, char *argv[])
{
    int ln, max, maxLineLength = 0;
    printf("Enter length of array: ");
    scanf("%i",&ln);
    printf("Enter length of max string: ");
    scanf("%i",&max);
    char *text[ln];

    for(int i=0; i<ln; i++)
    {
        text[i] = (char*)malloc(sizeof(char)*max);
        input(text[i], max, i);
    }

    int swap = sort(text, ln, &maxLineLength);

    for(int i=0; i<ln; i++)
    {
        output(text[i]);
    }

    printf("Swap count = %d\n", swap);
    printf("Max string length = %d\n", maxLineLength);

    return 0;
}
